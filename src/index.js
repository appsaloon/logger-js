/* eslint-disable no-extend-native */
/* eslint-disable camelcase */
import createLogstash from './util/logstash'
import getStackTrace, { removeFirstLine } from './util/getStackTrace'
import getCircularReplacer from './util/getCircularReplacer'

export const consoleOriginal = { ...console }

export default (options) => {
  const logEnv = process.env.REACT_APP_LOG_ENV || process.env.LOG_ENV
  if (Boolean(logEnv) || options.forceLogging === true) {
    // create a logstash instance from the logstashOptions object
    const {
      protocol = 'http',
      hostname = 'localhost',
      port = '8080',
      path = '', // app-frontend or app-backend
      site = typeof window !== 'undefined' ? window.location.hostname : 'unknown'
    } = options
    const logstashURI = `${protocol}://${hostname}:${port}/${path}`
    const tags = []
    const level = 'info'
    const logstashOptions = {
      muteConsole: true
    }
    const logstash = createLogstash(logstashURI, tags, level, logstashOptions)

    console.log(`logger-js: Logs will be transported to logstash at: ${logstashURI}`)

    // this function sends logs of any level to logstash
    // the console object is modified here to prevent infinite loops
    // as the logstash module writes to console
    const logstashSend = (level, message, data = {}) => {
      // level = level === 'log' ? 'info' : level
      data.site = site
      data.environment = logEnv
      logstash[level](message, data)
      // consoleOriginal.info('logger-js: should send to logstash: ', level, message, data)
    }

    // console.log('logger-js: logstashSend: ', logstashSend)

    // define all the levels for which the console functions will be overwritten
    const levels = [
      'info',
      'warn',
      'error'
    ]

    // this function overwrites a console function with custom added functionality
    const consoleModified = (level) => function () {
      // // do nothing if the message is caused by being unable to connect to the configured logstash instance.
      // if (typeof arguments[0] === 'object' && arguments[0].config && arguments[0].config.url === logstashURI) {
      //   consoleOriginal.error('logger-js: failed to send a log to the logstash instance', arguments[0])
      //   return
      // }

      // otherwise, send to logStash
      // consoleOriginal.log('logger-js: should send to the logstash instance', ...arguments)
      const message = [...arguments]
        .map((line) => JSON.stringify(line, getCircularReplacer()))
        .join('\n')
      const stack_trace = getStackTrace()
      consoleOriginal[level](`logger-js: ${level}: `, ...arguments)
      // consoleOriginal.info('logger-js: generated stack_trace: ', stack_trace)
      logstashSend(level, message, { stack_trace })
    }

    // overwrite the console function for every level
    levels.forEach((level) => {
      console[level] = (() => {
        return Function.prototype.bind.call(consoleModified(level), console)
      })()
      // consoleOriginal.info(`logger-js: Modified console.${level}`)
    })

    // listen for error events
    const catchError = function (event) {
      let message, stack_trace
      if (typeof window !== 'undefined') {
        message = event.error.message
        stack_trace = removeFirstLine(event.error.stack)
      } else {
        message = JSON.stringify(event, getCircularReplacer())
        try {
          const error = JSON.parse(message)
          if (error.message && error.stack) {
            message = error.message
            stack_trace = removeFirstLine(error.stack)
          }
        } catch (e) { }
      }
      consoleOriginal.log('logger-js: --message--', message)
      consoleOriginal.log('logger-js: --stack--', stack_trace)
      logstashSend('error', message, { stack_trace })
    }

    if (typeof window !== 'undefined') {
      window.addEventListener('error', catchError)
    } else {
      process.on('uncaughtException', catchError)
      if (!('toJSON' in Error.prototype)) {
        Object.defineProperty(Error.prototype, 'toJSON', {
          value: function () {
            var alt = {}
            Object.getOwnPropertyNames(this).forEach(function (key) {
              alt[key] = this[key]
            }, this)
            return alt
          },
          configurable: true,
          writable: true
        })
      }
    }
  } else {
    console.log('logger-js: Logs will NOT be transported to logstash')
  }
}
