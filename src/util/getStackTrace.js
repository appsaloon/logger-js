const getStackTrace = function () {
  try {
    const dummyError = {}
    Error.captureStackTrace(dummyError, getStackTrace)
    return removeFirstLine(dummyError.stack)
  } catch (e) { }
}
export default getStackTrace

export const removeFirstLine = function (stackTrace = '') {
  const stackTraceArray = stackTrace.split('\n')
  if (stackTraceArray.length > 1) {
    stackTraceArray.shift()
    return stackTraceArray.join('\n')
  } else {
    return stackTrace
  }
}
