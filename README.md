# Logger-js

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

## Description

Globally transports logs to a logstash instance:

* console.info
* console.warn
* console.error
* thrown errors

Works in react or node-js applications

## Usage

`npm i @appsaloon/logger-js`

### Node-js

In index.js:

```javascript
const initLogstash = require('@appsaloon/logger-js').default
const consoleOriginal = require('@appsaloon/logger-js').consoleOriginal
const logstashOptions = {
  // see logstashOptions
}
initLogstash(logstashOptions)
```

### Create-react-app

In index.js

```javascript
import initLogstash, { consoleOriginal } from '@appsaloon/logger-js'
const logstashOptions = {
  // see logstashOptions
}
initLogstash(logstashOptions)
```

### logstashOptions

* protocol
  * protocol of the logstash instance
  * type: string
  * default value: `http`
* hostname
  * hostname or ip of the logstash instance
  * type: string
  * default value: `localhost`
* port
  * port of the logstash instance
  * type: string, required
  * default value: `8080`
* path
  * pathname of the logstash instance
  * type: string
  * default value: ``
* site
  * source of the logs
  * type: string
  * default value: `window.location.hostname` (browser) or `unknown` (node-js)

### REACT_APP_LOG_ENV & LOG_ENV

logger-js will only send logs to the logstash instance when either of `REACT_APP_LOG_ENV` (create-react-app) or `LOG_ENV` (node-js) has been set.
This value must be any string

#### setting REACT_APP_LOG_ENV (create-react-app)

bitbucket-pipelines.yml:

```yaml
pipelines:
  branches:
    dev:
    - step:
        script:
        - docker build ... --build-arg REACT_APP_LOG_ENV=dev
        # ...
    production:
    - step:
        script:
        - docker build ... --build-arg REACT_APP_LOG_ENV=production
        # ...
```

Dockerfile:

```dockerfile
ARG REACT_APP_LOG_ENV
RUN REACT_APP_LOG_ENV=$REACT_APP_LOG_ENV npm run build
```

#### setting LOG_ENV (node-js)

docker-compose.yml:

```yaml
services:
  my-service-dev:
    environment:
    - LOG_ENV=local
  my-service-production:
    environment:
    - LOG_ENV=production
```

### Development

logger-js will also send logs to the logstash instance if `logstashOptions.forceLogging === true`
