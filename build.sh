#!/bin/bash

(rm -R dist || true)
babel src -d dist --copy-files
cp ./package*.json ./dist
cp README.md ./dist
cd dist
npm link